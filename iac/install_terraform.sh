#!/bin/sh

command -v curl >/dev/null 2>&1 || { echo >&2 "I require curl but it's not installed.  Aborting."; exit 1; }
command -v unzip >/dev/null 2>&1 || { echo >&2 "I require unzip but it's not installed.  Aborting."; exit 1; }


LATEST=`curl -s https://github.com/hashicorp/terraform/releases/latest|grep -Eoi '<a [^>]+>'|grep -Eo 'v[^\"]+'|grep -Eo '[^v\"]+'|grep -Eo "([0-9]{1,}\.)+[0-9]{1,}"`
VERSION=${LATEST:-0.11.13}

OS=$(uname | tr [:upper:] [:lower:])
URI=https://releases.hashicorp.com/terraform/${VERSION}/terraform_${VERSION}_${OS}_amd64.zip

show_commits() {
        curl -s https://api.github.com/repos/hashicorp/terraform/commits?per_page=10|jq  '.[]|{sha: .sha, message: .commit.message}'
        exit 0
}

_download() {

	if [ ! -d $HOME/bin ]; then
		mkdir $HOME/bin
	fi

	cd $HOME/bin \
		&& curl -fL -O $URI \
		&& unzip terraform_${VERSION}_${OS}_amd64.zip \
		&& rm terraform_${VERSION}_${OS}_amd64.zip

}

case "$1" in
	ls)
	show_commits
	;;
	*)
	_download
esac


