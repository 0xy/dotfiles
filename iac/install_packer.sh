#!/bin/bash
type -P curl || { echo >&2 "I require curl but it's not installed.  Aborting."; exit 1; }
type -P unzip || { echo >&2 "I require pip but it's not installed.  Aborting."; exit 1; }
TAGS=`curl -s https://github.com/hashicorp/packer/tags|grep -Eoi '<a [^>]+>'|grep -Eo '[^v\"]+'|grep -E "([0-9]{1,}\.)+[0-9]{1,}"|sed 's/.tar.gz//g;s/.zip//g'|sort -ur`
echo "$TAGS"


LATEST=`echo "$TAGS"|grep -m1 ""`
VERSION=${LATEST:-1.2.1}

OS=$(uname | tr [:upper:] [:lower:])
URI=https://releases.hashicorp.com/packer/${VERSION}/packer_${VERSION}_${OS}_amd64.zip

_download() {

	if [ ! -d $HOME/bin ]; then
		mkdir $HOME/bin
	fi

	cd $HOME/bin \
		&& curl -s -fL -O $URI \
		&& unzip packer_${VERSION}_${OS}_amd64.zip \
		&& rm packer_${VERSION}_${OS}_amd64.zip

}

_download

