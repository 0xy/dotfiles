#!/bin/bash
type -P go > /dev/null || { echo >&2 "I require go but it's not installed.  Aborting."; exit 1; }
type -P make > /dev/null || { echo >&2 "I require make but it's not installed.  Aborting."; exit 1; }
type -P curl > /dev/null || { echo >&2 "I require curl but it's not installed.  Aborting."; exit 1; }

if [ -d $HOME/gopath ]; then
	export GOPATH=$HOME/gopath
fi


pre_clean() {
	if [ -d $GOPATH/src/github.com/docker/infrakit ]; then
		rm -rvf $GOPATH/src/github.com/docker/infrakit
	fi
}

show_commits() {
	curl -s https://api.github.com/repos/docker/infrakit/commits?per_page=10|jq  '.[]|{sha: .sha, message: .commit.message}'
	exit 0
}

_install() {
	mkdir -p $GOPATH/src/github.com/docker/infrakit
	cd $GOPATH/src/github.com/docker \
		&& git clone $GIT_OPT -n https://github.com/docker/infrakit.git \
		&& cd infrakit \
		&& git checkout "$COMMIT"
	
	cd $GOPATH/src/github.com/docker/infrakit \
		&& make get-tools \
		&& make binaries

	mv -v ./build/* $HOME/bin	

}

case "$1" in
	ls)
		show_commits
		;;
	commit)
		export COMMIT="$2"
		pre_clean
		_install
		;;
	*)
		export LATEST=`curl https://api.github.com/repos/docker/infrakit/commits?per_page=1|jq -r .[0].sha`
		export COMMIT="$LATEST"
		export GIT_OPT="--depth=1"
		pre_clean
		_install
esac
