#!/bin/bash
set -x
set -e
if [ ! -d "$HOME/.dotfiles" ]; then
    git clone git@github.com:pandrew/.dotfiles.git $HOME/.dotfiles
fi




linkfiles_dev() {
    ln -vfs ~/.dotfiles/tmux.conf ~/.tmux.conf
    ln -vfs ~/.dotfiles/bash_profile ~/.bash_profile
    ln -vfs ~/.dotfiles/bashrc ~/.bashrc 
    ln -vfs ~/.dotfiles/vimrc ~/.vimrc
    ln -vfs ~/.dotfiles/gitconfig ~/.gitconfig
    ln -vfs ~/.dotfiles/gitignore ~/.gitignore
}
	#[ -d "$GOPATH/src/github.com/nsf/gocode/" ] && \
	#ln -vfs "$GOPATH/src/github.com/nsf/gocode/vim/autoload/gocomplete.vim" "$HOME/.vim/autoload/"
	#[ -d "$HOME/.vim/ftplugin/go" ] || mkdir -vp "$HOME/.vim/ftplugin/go"
    #ln -vfs "$GOPATH/src/github.com/nsf/gocode/vim/ftplugin/go/gocomplete.vim" "$HOME/.vim/ftplugin/go/"

set +x

case "$1" in
    "dev")
        linkfiles_dev
        ;;
    *)
        echo "invalid option"
        echo "valid options are: 'dev'"
esac

