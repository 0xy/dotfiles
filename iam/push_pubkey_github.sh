#!/bin/bash
# Push public ssh key to Github if you have an access token
set -x
PUBKEY=`ssh-keygen -y -f $HOME/.ssh/id_rsa`
curl -i -H "Content-Type: application/json" -X POST -d '{"title": "testkey1","key": '\""$PUBKEY"\"'}' https://api.github.com/user/keys?access_token=$1
