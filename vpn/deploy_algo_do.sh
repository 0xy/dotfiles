#!/bin/bash
type -P ansible-playbook || { echo >&2 "I require ansible but it's not installed.  Aborting."; exit 1; }
type -P git || { echo >&2 "I require git but it's not installed.  Aborting."; exit 1; }
set -x



git clone https://github.com/trailofbits/algo \
	&& cd algo \
	&& ansible-playbook deploy.yml -t digitalocean,vpn,cloud -e "do_access_token="${DOTOKEN}" do_server_name=algo.local do_region=ams2"
