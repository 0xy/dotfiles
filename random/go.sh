#!/bin/bash

set -e

go get github.com/jstemmer/gotags
go get github.com/rogpeppe/godef
go get golang.org/x/tools/cmd/goimports
go get -u github.com/monochromegane/the_platinum_searcher/...
go get -u github.com/hugows/hf
go get github.com/golang/lint/golint
go get github.com/Rican7/define
go get -v github.com/gohugoio/hugo
go get go.universe.tf/netboot/cmd/pixiecore
