#!/usr/bin/env bash
#set -x
OS="$(uname)"
if [ "$OS" == "Linux" ]; then
        export SHA=sha256sum
elif [ "$OS" == "OpenBSD" ]; then
        export SHA=sha256
else
        exit 1
fi

dirs=($(ls -d */))
for dir in "${dirs[@]}"
do
         if  [ ! -f "${dir%/}".sha ]
         then
                 find ./"$dir" -type f  -print0 | xargs -0 -I{} $SHA  "{}"  |tee ./"${dir%/}".sha
         fi
done
