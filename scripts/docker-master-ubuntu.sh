#!/bin/bash

set -e

# install system dependencies 
apt-get install -y \
    htop \
    git \
    make \
    curl \
    supervisor \
    cgroup-lite \
    aufs-tools


# Add ourself to docker group
groupadd docker || true
gpasswd -a $(whoami) docker || true


if [ ! -d $HOME/.bin ]; then
        mkdir $HOME/.bin
fi



# Pull docker from master
curl -fL -o $HOME/.bin/docker https://master.dockerproject.com/linux/amd64/docker

# Ensure docker is stoped
supervisorctl stop docker | true
# install docker from master
curl -SsL -o /usr/local/bin/docker https://master.dockerproject.com/linux/amd64/docker
chmod +x /usr/local/bin/docker

# Pull supervisorctl config
curl -o /etc/supervisor/conf.d/docker.conf https://raw.githubusercontent.com/pandrew/.dotfiles/master/docker.conf

# DANGER! CLEAN docker graph
rm -rvf /var/lib/docker

# Ensure docker is started
supervisorctl start docker


# Ensure we can run docker
docker version
