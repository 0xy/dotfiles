#!/bin/bash
[[ -z "$DEBUG" ]] || set -x
set -x

command -v curl >/dev/null 2>&1 || { echo >&2 "I require curl but it's not installed.  Aborting."; exit 1; }

export OS=$(uname -s | tr '[:upper:]' '[:lower:]')
export GOROOT=$HOME/go
export GOPATH=$HOME/gopath
GO_VERSION=1.17.5


if [ -d "$GOROOT" ]; then
        rm -rvf "$GOROOT"
else
        mkdir -p "$GOROOT"
fi

curl -fL https://dl.google.com/go/go${GO_VERSION}.${OS}-amd64.tar.gz -o $HOME/go.tar.gz
tar xvf $HOME/go.tar.gz -C $HOME

rm -v $HOME/go.tar.gz
