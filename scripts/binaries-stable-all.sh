#!/bin/bash

if [ ! -d $HOME/.bin ]; then
    mkdir $HOME/.bin
fi

UNAMELOWER=`uname | tr '[:upper:]' '[:lower:]'`



buildOtto() {
    set -x
    (
    OTTO_SRC="$GOPATH/src/github.com/hashicorp/otto"
    [ ! -d "$OTTO_SRC" ] || rm -rf "$OTTO_SRC"
    git clone --depth=1 https://github.com/hashicorp/otto.git "$OTTO_SRC"
    )
}




compose() {
    COMPOSE_VERSION=1.5.0
    echo Pulling compose $COMPOSE_VERSION
    curl -fL -o $HOME/.bin/compose https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-$(uname)-x86_64 || exit 1
}


buildMasterTerraform() {
    set -x
    (
    docker rm -f terraform || true
    docker rmi -f terraform:build || true
    TERRAFORM_SRC="$GOPATH/src/github.com/hashicorp/terraform"
    rm -rf "$TERRAFORM_SRC" \
        && git clone --depth=1 https://github.com/hashicorp/terraform.git "$TERRAFORM_SRC" \
        && git -C "$TERRAFORM_SRC" checkout -b build_with_docker

    OPT="${1:-bin}"
    OS="${2:-darwin}"
    ARCH="${3:-amd64}"

    cat <<-EOF > "$TERRAFORM_SRC/Dockerfile.build"
	FROM golang:1.5.1
	ENV XC_OS="${OS}"
	ENV XC_ARCH="${ARCH}"
	RUN apt-get update && apt-get -y install zip
	COPY . /go/src/github.com/hashicorp/terraform

	RUN cd /go/src/github.com/hashicorp/terraform \\
	    && make updatedeps \\
	    && make "${OPT}"
	EOF
    
    docker build --rm --force-rm -t terraform:build -f "$TERRAFORM_SRC/Dockerfile.build" .

    # Cant copy data from an image so we have to run the container once to extract the binaries
    docker run --name terraform terraform:build

    docker cp terraform:/go/src/github.com/hashicorp/terraform/pkg/darwin_amd64/ - | tar -C $HOME/.bin/ --strip 1 -xvf -
    )
    set +x
}


buildMasterPixiecore() {
    set -x
    PIXIECORE_SRC="$GOPATH/src/github.com/danderson/pixiecore"
    rm -rf "$PIXIECORE_SRC"
    git clone --depth=1 https://github.com/danderson/pixiecore.git "$PIXIECORE_SRC"
    cd "$PIXIECORE_SRC" \
        && go install
    set +x
}

pullPacker() {
    set -x
    PACKER_VERSION=0.8.6
    (echo "Removing old packer" \
        && rm -vf $HOME/.bin/packer* \
        && echo "Downloading new packer" \
        && curl -fL -o $HOME/.bin/packer.zip https://dl.bintray.com/mitchellh/packer/packer_${PACKER_VERSION}_${UNAMELOWER}_amd64.zip || exit 1\
        && cd $HOME/.bin \
        && unzip -o packer.zip \
        && rm -f packer.zip \
        && chmod +x packer*)
    set +x
}
