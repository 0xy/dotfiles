#!/bin/bash


set -xe

export DOCKER_PLEX="${DOCKER_PLEX:-plex}"

plex_backup() {
	docker exec "$DOCKER_PLEX" tar Ccvf \
		. - "/var/lib/plexmediaserver/Library/Application Support" \
		--exclude "/var/lib/plexmediaserver/Library/Application\ Support/Plex\ Media\ Server/Cache" | \
		cat - > plex-`date +%F`.tar
}


plex_restore() {
	cat plex-`date +%F`.tar  | docker exec -i "$DOCKER_PLEX" tar xvf -
}

$@
