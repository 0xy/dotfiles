#!/bin/bash
[[ -z "$DEBUG" ]] || set -x
set -e

downloadDocker() {

	if [ "$type" == "stable" ];then
		echo Pulling docker $DOCKER_GET_VERSION
		DOCKER_GET_VERSION="$(curl -s https://api.github.com/repos/docker/docker/releases/latest | grep tag_name | cut -d '"' -f 4 | cut -c 2-)"
		curl -fL https://get.docker.com/builds/$(uname)/x86_64/docker-${DOCKER_GET_VERSION}.tgz | tar xvf - -C ~/.bin/ --strip=1 docker/docker
	elif [ "$type" == "pre-release" ];then
		DOCKER_TEST_VERSION="$(curl -s https://api.github.com/repos/docker/docker/releases | grep tag_name | grep rc | cut -d '"' -f 4 | cut -c 2- | head -n 1)"
		echo Pulling $DOCKER_TEST_VERSION
		curl -fL https://test.docker.com/builds/$(uname)/x86_64/docker-${DOCKER_TEST_VERSION}.tgz | tar xvf - -C ~/.bin/ --strip=1 docker/docker
	fi

}

case "$1" in
	download)
		case "$2" in
			stable)
				type="stable"
				;;
			pre-release)
				type="pre-release"
				;;
		esac
		downloadDocker
		;;
esac


