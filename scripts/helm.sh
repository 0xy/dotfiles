#!/bin/bash
curl -fL https://kubernetes-helm.storage.googleapis.com/helm-v2.8.2-linux-amd64.tar.gz | tar xzv --strip=1 linux-amd64/helm
chmod +x helm
mv helm $HOME/bin/
