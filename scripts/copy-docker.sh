#!/bin/bash
set -x
set -e

copyDocker() {
    docker_bins=(cross binary build-deb)
    for bin in ${docker_bins[@]}
    do
        lookup_bin=$(docker inspect -f '{{.Args}}' docker | grep -wo "$bin")
        if [ "$lookup_bin" == "cross" ]
        then
            DOCKER_VERSION=$(< $GOPATH/src/github.com/docker/docker/VERSION)
            docker cp docker:/go/src/github.com/docker/docker/bundles/${DOCKER_VERSION}/cross/darwin/amd64/docker-${DOCKER_VERSION} $HOME/.bin/ \
                && chmod +x $HOME/.bin/docker-${DOCKER_VERSION}
        elif [ "$lookup_bin" == "build-deb" ]
        then
            DEB_VERSION="${DEB_VERSION:-ubuntu-trusty}"
            docker cp docker:/go/src/github.com/docker/docker/bundles/${DOCKER_VERSION}/build-deb/${DEB_VERSION}/ - | tar -C $HOME -xvf - *.deb || true
        elif [ "$lookup_bin" == "binary" ]
        then
            docker cp docker:/go/src/github.com/docker/docker/bundles/${DOCKER_VERSION}/binary/docker-${DOCKER_VERSION} $HOME/Dropbox/Public/docker || true
        else
            echo "Found no binary"
        fi
    done
}

copyDocker
