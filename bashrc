!/bin/bash



# Basics
: ${HOME=~}
: ${LOGNAME=$(id -un)}
: ${UNAME=$(uname)}

# Complete hostnames from this file
: ${HOSTFILE=~/.ssh/known_hosts}


#-------------------------------------------------------------------------------
# Shell Options
#-------------------------------------------------------------------------------

# System bashrc
#test -r /etc/bash.bashrc && . /etc/bash.bashrc

# Notify bg task completion immediately
set -o notify

# No mail notifications
unset MAILCHECK

# default umask
umask 0022

# Terminal type
case $UNAME in
    CYGWIN* | MINGW32*)
        export TERM=cygwin
        ;;
    *)
        export TERM=xterm-256color
        ;;
esac

#-------------------------------------------------------------------------------
# Path
#-------------------------------------------------------------------------------

PATH="/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin"
PATH="$HOME/bin:/usr/local/bin:$PATH"
PATH="$PATH:$HOME/texlive/2020/bin/x86_64-linux"
PATH="$PATH:$HOME/bin/bin"
LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/home/paul/Downloads/velocidrone"


# OS-Specific stuf
case $UNAME in
    Darwin)
        export CLICOLOR=1
        export LSCOLORS=ExFxCxDxBxegedabagacad
	alias battery="echo -e '\n' && pmset -g batt | cut -f2 && echo -e '\n'"
        ;;
    Linux)
        alias lsa='ls -lah --color=auto'
        ;;
esac


[ -f ~/.gpg-agent-info ] && source ~/.gpg-agent-info
if [ -S "${GPG_AGENT_INFO%%:*}" ]; then
  export GPG_AGENT_INFO
else
  eval $( gpg-agent --daemon --write-env-file ~/.gpg-agent-info )
fi


rsync_via_bastion() {
	set -x
	bastion=$1
	#rsync -avzP -e 'ssh -A -o ProxyCommand="ssh -W %h:%p"' $bastion $source $destination
	rsync -avzP -e 'ssh -A -o "ProxyCommand ssh -W %h:%p "$1""' "$2" "$3"
}

find_by_size() {
	# find . -type f -size +2M -exec ls -lh {} \;
	find . -type f -size +$1M -exec ls -lh {} \;
}

osx_enable_ssh() {
	sudo systemsetup -setremotelogin on
	sudo systemsetup -getremotelogin
}

osx_disable_ssh() {
	sudo systemsetup -setremotelogin off
	sudo systemsetup -getremotelogin
}

objdump2shellcode() { 
	for i in $(objdump -d "$1" |grep "^ " |cut -f2); do echo -n '\x'$i; done; echo 
}

socks() {
    SOCKSPORT=31001
    echo "Port set to $SOCKSPORT"
    ssh -D $SOCKSPORT $1 -N
}

ssh_force_password () {
    ssh -o PreferredAuthentications=password -o PubkeyAuthentication=no "$1"
}

osx_sleep_never() {
	sudo systemsetup -setcomputersleep Never
}

osx_sleep_default() {
	sudo systemsetup -setcomputersleep 1
}

kali() {
	curl -s http://cdimage.kali.org/kali-weekly/|grep -Eoi '<a [^>]+>'| grep -Eo 'href="[^\"]+"' |grep kali|cut -d'"' -f2|sed 's|^|http://cdimage.kali.org/kali-weekly/|g'
}

function docker-volume-backup-compressed() {
  docker run --rm -v /tmp:/backup --volumes-from "$1" debian:jessie tar -czvf /backup/backup.tar.gz "${@:2}"
}

function docker-inspect-volume() {
	docker inspect --format '{{ range .Mounts }}{{ .Name }} {{ end }}' "$1"
}

function setgpufanspeed() {
	#nvidia-xconfig --cool-bits=4
	
	GPU_FANSPEED=$1
	nvidia-settings -a "[gpu:0]/GPUFanControlState=1" -a "[fan:0]/GPUTargetFanSpeed=$GPU_FANSPEED"
}

function freemem() {
    free --si -h | awk '{print $4}' | awk 'FNR == 2 {print}'
}

export GOROOT=$HOME/go
if [ ! -d "$GOROOT" ]; then
	export GOROOT=/usr/local/go
fi
export GOPATH=$HOME/gopath
export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"

#-------------------------------------------------------------------------------
# Env. Configuration
#-------------------------------------------------------------------------------

# detect interactive shell
case "$-" in
    *i*) INTERACTIVE=yes ;;
    *)   unset INTERACTIVE ;;
esac

# detect login shell
case "$0" in
    -*) LOGIN=yes ;;
    *)  unset LOGIN ;;
esac

# Proper locale
: ${LANG:="en_US.UTF-8"}
: ${LANGUAGE:="en"}
: ${LC_CTYPE:="en_US.UTF-8"}
: ${LC_ALL:="en_US.UTF-8"}
export LANG LANGUAGE LC_CTYPE LC_ALL

# Always use passive mode FTP
: ${FTP_PASSIVE:=1}
export FTP_PASSIVE

# Ignore backups, CVS directories
FIGNORE="~:CVS:#:.pyc"
HISTCONTROL=ignoreboth

# Make pinentry work
GPG_TTY=`tty`
export GPG_TTY


#-------------------------------------------------------------------------------
# Editor and Pager
#-------------------------------------------------------------------------------
EDITOR="vim"
export EDITOR

PAGER="less -FirSwX"
MANPAGER="$PAGER"
export PAGER MANPAGER

#-------------------------------------------------------------------------------
# Aliases / Functions
#-------------------------------------------------------------------------------
alias ls='ls -alh'
alias destructive_play_music_interactive='find . -type f \( -name \*.m4a -o -name \*.ogg -name \*.flac -o -name \*.m3a -o -name \*.mp3 \) -exec mpv --cache=50000 --cache-initial=10000 --audio-display=no {} \; -exec rm -iv {} \;'
#alias destructive_play_music_noninteractive='find . -type f \( -name \*.wv -o -name \*.m4a -o -name \*.flac -o -name \*.m3a -o -name \*.mp3 \) -exec mpv --cache=yes  {} \; -exec rm -v {} \;'
destructive_play_music_noninteract() {
	if [ -z "$1" ]; then
		_files="$(find . -type f -name \*.mp3)"
	else
		_FINDFILES='-name \*.$1'
	fi
	#find . -type f \( -name \*.m4a -o -name \*.flac -o -name \*.m3a -o -name \*.mp3 \) -exec mpv --cache=yes {} +
	#_files="$(find . -type f \(  "$_FINDFILES" \) \;)"
	for _f in _files
	do
		mpv --cache=yes "$_f" && echo ok
	done
	
}


#alias destructive_play_music_noninteractive='DEFAULT_FINDFILES="-name \*mp3"; find . -type f \( ${DEFAULT_FINDFILES} \) -exec mpv --cache=yes  {} \; -exec echo ok \;'
alias play_music_noninteractive='find . -type f \( -name \*.m4a -o -name \*.flac -o -name \*.m3a -o -name \*.mp3 \) -exec mpv --cache=yes {} \;'
#alias destructive_play_music_andnoninteractive='find . -type f \( -name \*.wv -o -name \*.m4a -o -name \*.flac -o -name \*.m3a -o -name \*.mp3 \) -exec mpv --cache=yes  {} &&  rm -v {} \;'

alias gs='git stash'
alias gp='git push'
alias gc='git commit -vp'


# Usage: puniq [path]
# Remove duplicate entries from a PATH style value while
# retaining the original order.
puniq() {
    echo "$1" |tr : '\n' |nl |sort -u -k 2,2 |sort -n |
    cut -f 2- |tr '\n' : |sed -e 's/:$//' -e 's/^://'
}

#-------------------------------------------------------------------------------
# SSH Agent
#-------------------------------------------------------------------------------
SSH_ENV=$HOME/.ssh/environment
function start_ssh_agent {
     ssh-agent | sed 's/^echo/#echo/' > ${SSH_ENV}
     chmod 0600 ${SSH_ENV}
     . ${SSH_ENV} > /dev/null
     ssh-add
}

# Source SSH agent settings if it is already running, otherwise start
# up the agent proprely.
function bootstrap_agent {
    if [ -f "${SSH_ENV}" ]; then
        . ${SSH_ENV} > /dev/null
        # ps ${SSH_AGENT_PID} doesn't work under cywgin
        ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
            start_ssh_agent
        }
    else
         start_ssh_agent
    fi

    . $HOME/.dotfiles/ssh-find-agent.sh
}

# ssh-agent is blocking agent forwarding from other hosts.
# Start agent only if theres no socket.
if [ -z "${SSH_AUTH_SOCK}" ]; then
    bootstrap_agent
else
    ssh-add
fi

# stop agent upon logout
trap 'test -n "$SSH_AGENT_PID" && eval `ssh-agent -k`' 0



#broken?
gb() {
	git name-rev --name-only @ 2>/dev/null
}

# Set default prompt if interactive
if [ -n "$PS1" ]
then
	if [ -z "$SSH_CLIENT" ]
	then
		export PS1="\h \$(gb) \$ "
	else
		export PS1="(ssh) \h \$(gb) \$ "
	fi
fi

complete -C /usr/local/bin/mc mc
. "$HOME/.cargo/env"
